<?php
foreach ($detail_karyawan as $data) {
	$nik  = $data->nik;
	$nama_lengkap  = $data->nama_lengkap;
	$tempat_lahir  = $data->tempat_lahir;
	$tgl_lahir  = $data->tgl_lahir;
	$jenis_kelamin  = $data->jenis_kelamin;
	$alamat  = $data->alamat;
	$telp  = $data->telp;
	$kode_jabatan  = $data->kode_jabatan;
	$photo  = $data->photo;
}
     //pisah tanggal bualn tahun
	 $thn_pisah = substr($tgl_lahir, 0, 4);
	 $bln_pisah = substr($tgl_lahir, 5, 2);
	 $tgl_pisah = substr($tgl_lahir, 8, 2);

	?>
<table width="50%" height="78" align="center">
 
  <center><h2 style="font-family:'Comic Sans MS', cursive">Edit Karyawan</h2></center>
 
   <div style="color: red" align="center"><?= validation_errors(); ?></div>
<form action="<?=base_url()?>karyawan/editkaryawan/<?= $nik; ?>" method="POST" enctype="multipart/form-data">
<table width="35%" cellspacing="3" cellpadding="8" align="center" bgcolor="#00FF00">
  <tr>
    <br />  
    <td>Nik</td>
    <td>:</td>
    <td>
      <input value="<?= $nik; ?>" type="text" name="nik" id="nik" readonly></td>
  </tr>
  <tr>
    <td>Nama</td>
    <td>:</td>
    <td>
      <input value="<?= $nama_lengkap; ?>" type="text" name="nama_lengkap" id="nama_lengkap" maxlength="50"></td>
  </tr>
  <tr>
    <td>Tempat Lahir</td>
    <td>:</td>
    <td> <input value="<?= $tempat_lahir; ?>" type="text" name="tempat_lahir" id="tempat_lahir" maxlength="50"></td>
    </td>
  </tr>
   <tr>
    <td>Tanggal Lahir</td>
    <td>:</td>
    <td><select name="tgl" id="tgl">
    <?php
	for($tgl=1;$tgl<=31;$tgl++){
		$select_tgl = ($tgl == $tgl_pisah) ? 'selected' : '';
		?>
        <option value="<?=$tgl;?>" <?=$select_tgl;?>><?=$tgl; ?></option>
        <?php
	}
	?>
    </select>
      <select name="bln" id="bln">
       <?php
	   $bln_n = array('januari','februari','maret','april','mei','juni','juli','agustus','september','oktober','november','desember');
	for($bln=0;$bln<12;$bln++){
		$select_bln = ($bln == $bln_pisah) ? 'selected' : '';
		?>
       
        <option value="<?=$bln+1;?>" <?= $select_bln; ?>>
		<?=$bln_n[$bln];?>
        </option>
        <?php
	}
	?>
      </select>
      <select name="thn" id="thn">
       <?php
	for($thn=date('Y')-60;$thn<=date('Y')-15;$thn++){
	    $select_thn = ($thn == $thn_pisah) ? 'selected' : '';

		?>
        <option value="<?=$thn;?>" <?= $select_thn; ?>><?=$thn;?></option>
        <?php
	}
	?>
      </select>
      </td>
  </tr>
  <tr>
    <td height="35">Jenis Kelamin</td>
    <td>:</td>
    <td><select name="jenis_kelamin" id="jenis_kelamin">
       <?php
	if($jk == 'P'){
		$slc_p = 'selected';
		$slc_l = '';
	}else if($jk == 'L'){
		$slc_l = 'selected';
		$slc_p = '';
	}else{
		$slc_p = '';
		$slc_l = '';
		}
	?>
     <option <?=$slc_p;?> value="p">Perempuan</option>
      <option  <?=$slc_l;?> value="l">laki-laki</option>
      
    </select>
    </td>
  </tr>
   <tr>
    <td>Alamat</td>
    <td>:</td>
    <td><textarea name="alamat" id="alamat" cols="45" rows="5" ><?= $alamat; ?></textarea></td>
  </tr>
 
  <tr>
    <td>Telepon</td>
    <td>:</td>
    <td><input value="<?= $telp; ?>" type="text" name="telp" id="telp"></td>
    </td>
  </tr>
 
 
 <tr>
    <td height="35">Jabatan</td>
    <td>:</td>
    <td><select name="kode_jabatan" id="kode_jabatan">
    <?php foreach($data_jabatan as $data) {
		      $select_jabatan = ($data->kode_jabatan ==
			  $kode_jabatan) ? 'selected' : '';
		?>
       <option value="<?= $data->kode_jabatan;?>" <?=$select_jabatan; ?>>
       <?= $data->nama_jabatan; ?></option>
       
      
      <?php }?>
      
    </select>
    </td>
  </tr>
  <tr>
    <td style="text-align:left;">Upload Foto</td>
    <td>:</td>
    <td style="text-align:left;">
    <input type="file" name="image" id="image">
      <input type="hidden" name="foto_old" id="foto_old" value="<?= $photo; ?>">
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><input type="submit" name="simpan" id="simpan" value="simpan" style="background-color:#06F">
      <input type="submit" name="batal" id="batal" value="reset" style="background-color:#F00">
      <br></br>
      <a href="<?=base_url();?>karyawan/listkaryawan"><input type="button" name="kembali ke menu sebelumnya" id="kembali ke menu sebelumnya" value="kembali ke menu sebelumnya" style="background-color:#0FF"></a></td>
  </tr>
</table>
</table>
</form>
