<table align="center">
<tr>
    <td width="1233" height="61" align="center">
      <p><b> Laporan Data Pembeliaan</p>
      <p><b>Dari Tanggal 2019-02-01 s/d 2019-04-30</p>
    </td>
  </tr>
<tr>
<td><a href="<?=base_url();?>pembelian/dtp" style="text-decoration:none;"><< Kembali...</a></td>
</tr>
<table width="68%" border="1" bordercolor="#CCCCCC" cellspacing="0" cellpadding="7" align="center">
  <tr align="center" style="color:#FFF" bgcolor="#000000" >
    <td>No</td>
    <td>ID Pembelian</td>
    <td>Nomor Transaksi</td>
   	<td>Tanggal</td>
    <td>Total Barang</td>
    <td>Total Qty</td>
    <td>Jumlah Nominal Pembelian</td>
    

 </tr>
  <?php
  $no = 0;
   $total_keseluruhan = 0;

    foreach ($data_pembelian as $data) {
		$no++;

?>
<tr align="center">
   
    <td><?=$no;?></td>
    <td><?= $data->id_pembelian_h; ?></td>
	  <td><?= $data->no_transaksi; ?></td>
    <td><?= $data->tanggal; ?></td>
    <td><?= $data->stok; ?></td>
    <td><?= $data->qty; ?></td>
    <td>RP. <?= number_format($data->jumlah); ?></td>
</tr>
<?php 
		//menghitung total
		$total_keseluruhan+= $data->jumlah;
	}
?>
</table>

<table width="68%" cellspacing="0" cellpadding="7" align="center">
<tr bgcolor="#00FFFF" align="right">
<td>Total Keseluruhan Pembelian &emsp; Rp.<?=number_format($total_keseluruhan); ?></td>
 </tr>
 </table>
  
